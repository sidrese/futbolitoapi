var Player = require('../models/player');

module.exports = {

    getAll: function (req,res,next) {
        Player.find({}).lean().exec(function (err,players) {
            res.json({
                succes: true,
                players: players
            })
        })
    },
    save: function (req, res, next) {
        var player = new Player({
            name: req.body.player.name
        });
        player.save(function (err) {
            res.json({
                succes: true,
                player: player
            });
            rank.push(player);
        });
    },
    delete: function (req, res, next) {
        Player.findById(req.body.player._id).remove().exec();
        res.json({
            succes: true
        })
        var index = rank.findIndex(function (e) {
            return e._id == req.body.player._id
        })-1;
        rank.splice(index,1);
    },
    update: function (req, res, next) {
        Player.findById(req.body.player._id,function (err,player) {
            player.name = req.body.player.name;
            player.save();
        });
    }

}