var Player = require('../models/player');

rank = [];

Player.find({}).lean().exec(function (err,players) {
    rank = players;
})

module.exports = {

    get: function (req,res, next) {
        res.json({
            success: true,
            rank: rank
        })
    },

    update: function (req, res, next) {
        var player = req.body.player;
        var actualIndex = rank.findIndex(function (e) {
            return e._id == req.body.player._id;
        });

        var toIndex = req.body.to;
        rank.splice(actualIndex,1);
        rank.splice(toIndex,0,player);
        res.json({
            success: true
        })
    }

}