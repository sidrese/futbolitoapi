var express = require('express');
var router = express.Router();
var player = require('../middleware/player-mid');
var rank = require('../middleware/rank-mid');


router.get('/player', [player.getAll]);
router.post('/player', [player.save]);
router.put('/player', [player.update]);
router.delete('/player', [player.delete]);

router.get('/rank', [rank.get]);
router.put('/rank', [rank.update]);

module.exports = router;
