var express = require('express');
var router = express.Router();
var config = require('../config/config');


router.get('/', function (req, res, next) {
    res.sendFile('index.html', config.view_options);
});

module.exports = router;
