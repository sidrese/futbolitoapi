module.exports = {

    apps: [

        {
            name: "Futbolito API",
            script: "bin/www",
            env: {
                NODE_ENV: "development"
            },
            env_production: {
                NODE_ENV: "production"
            }
        }
    ],

    deploy: {
        labs: {
            user: "deploy",
            host: "54.208.239.0",
            ref: "origin/develop",
            repo: "https://enrique_si:Quique_58@bitbucket.org/sidrese/futbolitoapi.git",
            path: "/home/deploy/futbolito/api",
            "post-deploy": "npm install && pm2 startOrRestart ecosystem.config.js --env dev",
            env: {
                NODE_ENV: "development"
            }
        }
    }
}
